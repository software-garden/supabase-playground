{
  description = "The Supabase playground of Tad and Sam";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.git
            pkgs.gnumake
            pkgs.elmPackages.elm
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "supabase-playgroud";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "supabase-playgroud-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
            pkgs.miniserve
            pkgs.jq
            pkgs.httpie
            pkgs.elmPackages.elm-language-server
            pkgs.elmPackages.elm-format
            pkgs.elmPackages.elm-test
          ];
        };

      }
    );
}
