module Main exposing (main)

import Html exposing (Html)
import Html.Attributes
import Html.Custom as Html
import Html.Events
import Browser


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { emailAddressInput : String
    }



-- INIT


type alias Flags =
    {}


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { emailAddressInput = "" }
    , Cmd.none
    )



-- UPDATE


type Msg
    = EmailInputChanged String
    | LoginFormSubmitted


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EmailInputChanged value ->
            ( { model | emailAddressInput = value }
            , Cmd.none
            )

        LoginFormSubmitted ->
            ( model
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = ""
    , body =
        [ viewHeader
        , viewMain
        ]
    }


viewHeader : Html Msg
viewHeader =
    [ "Supabase + Elm" |> Html.text |> List.singleton |> Html.h1 []
    , "Sign in via magic link with your email below" |> Html.text |> List.singleton |> Html.p []
    ]
        |> Html.header [ Html.Attributes.class "container" ]


viewMain =
    [ viewLoginForm ]
        |> Html.main_ [ Html.Attributes.class "container" ]
        


viewLoginForm : Html Msg
viewLoginForm =
    [ Html.textInput "Email" EmailInputChanged ""
    , "Send magic link" |> Html.text |> List.singleton |> Html.button []
    ]
        |> Html.form [ Html.Events.onSubmit LoginFormSubmitted ]
